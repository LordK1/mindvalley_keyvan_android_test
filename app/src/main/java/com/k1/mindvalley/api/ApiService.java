package com.k1.mindvalley.api;

import com.k1.mindvalley.model.Photo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * The correct link in this test is the following: http://pastebin.com/raw/wgkJgazE
 * <p/>
 * Created by K1 on 7/8/16.
 */
public interface ApiService {

    @GET("raw/wgkJgazE")
    Call<ArrayList<Photo>> getList();
}
