package com.k1.mindvalley.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.k1.mindvalley.MainActivity;
import com.k1.mindvalley.R;
import com.k1.mindvalley.holder.PhotoViewHolder;
import com.k1.mindvalley.model.Photo;

import java.util.ArrayList;

/**
 * To make adaptive {@link com.k1.mindvalley.model.Photo} items into {@link MainActivity#mRecyclerView}
 * and used {@link PhotoAdapter}
 */
public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Photo> photos;

    public PhotoAdapter(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item_view_holder, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PhotoViewHolder) holder).onBind(getItem(position));
    }

    /**
     * get {@link Photo} from list
     *
     * @param position
     * @return
     */
    private Photo getItem(int position) {
        return photos.get(position);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }


}
