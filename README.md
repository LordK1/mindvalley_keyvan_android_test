# Mindvalley 
This is practical challenge with some defined features for apply job in Mindvalley and the role of Android Developer.
These below parameters are most important keynotes for coding in this project. 
 
 1. Readability:
 Class and method names should clearly show their intent and responsibility.
  
 2. Maintainability:
 “SOLID” Principles and design pattern.
 MVC model
 We want to see how you use and integrate 3rd party libraries
 
  
 3. Scalability:
 Your software should easily accommodate possible future requirement changes (example: if you are asked to change to xml-based api instead of json)
 
 
 4. Testability:
 Please Unit Test your classes. In our assignment, you can just test all non-UI classes.
  
# DEMONSTRATION:
As a developer I know some parts of this assigned task (to make module for future usage with SOLID principal to handle some grate usability scenarios)
are missed, but I just try to make some codes and simple sample android during this short time.
Although It may be very time-consuming, to make some infrastructures modules for use in other/upper layers in projects .
You need something like Retrofit or ION libraries these are very big and large-scaled third parties to handle 
all you need to do about network in an android application, so if you want to create something like these 
libraries you need to know what exactly to do or want.
I mean, I can make some modules like above third parties but at first I need whole bunch of details and documented requirements
and at second I need scheduled timing and deadlines according to scale of this task.
I hope you never think about I don't want or I can't make something like that, and also in this simple sample project 
I just try to make something valuable during this short period time.
Good Luck.
 



# Used Libraries :
- Retrofit 2.0 (http://square.github.io/retrofit/)
- Google GSON 2.7 (https://github.com/google/gson)
- Picasso ()
- Parcels ()
- Android Support Design ()
- Espresso 2.0 ()